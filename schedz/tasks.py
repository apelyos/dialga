﻿"""
This file contains async tasks to be executed server-side.
What you're about to see is possibly the worst way to implement such tasks.
The best practice will be to implement Celery & Redis to perform task queue management.
"""

import datetime, time, requests
import threading
from .models import Slot, GlobalMessage, Schedule, SchedUser
from django.shortcuts import get_object_or_404
from social.apps.django_app.utils import load_strategy


class google_calendar_mgmt(threading.Thread):
    def __init__(self, user, schedule, slot, action='create'):
        threading.Thread.__init__(self)
        self.user = user
        self.schedule = schedule
        self.slot = slot
        self.action = action
        self.event = {
            'summary': "{} Time Slot".format(self.schedule.name),
            'location': self.schedule.location,
            'description': "Created by Dialga scheduling system (c)",
            'start': {
                'dateTime': self.slot.abs_start_time().isoformat(),  # '2015-05-28T09:00:00-07:00',
                'timeZone': 'Israel',
            },
            'end': {
                'dateTime': self.slot.abs_end_time().isoformat(),  # '2015-05-28T17:00:00-07:00',
                'timeZone': 'Israel',
            },
        }

    def run(self):
        sched_user = get_object_or_404(SchedUser, user_relation=self.user)

        if not sched_user.auto_schedule:
            return

        social = self.user.social_auth.get(provider='google-oauth2')

        if self.do_action(social) == 401: # we got HTTP UNAUTHORIZED
            print("refreshing token & trying again")
            strategy = load_strategy()
            social.refresh_token(strategy=strategy)
            res = self.do_action(social)
            print("response after refresh: %d" % res)

    def do_action(self, social):
        response = []
        if self.action == 'create':
            response = requests.post(
                'https://www.googleapis.com/calendar/v3/calendars/primary/events',  # primary = calendarID
                params={'access_token': social.extra_data['access_token']},
                json=self.event,
            )

            if response.status_code == 200:
                self.slot.google_event_id = response.json()['id']
                self.slot.save()
            else:
                print("google calendar create failed")
                #print(response)
        elif self.action == 'delete':
            if self.slot.google_event_id:
                response = requests.delete(
                    "https://www.googleapis.com/calendar/v3/calendars/primary/events/{}".format(
                        self.slot.google_event_id),  # primary = calendarID
                    params={'access_token': social.extra_data['access_token']},
                )

                if response.status_code == 200 or response.status_code == 204:
                    self.slot.google_event_id = None
                    self.slot.save()
                else:
                    print("google calendar delete failed")
                    #print(response)
        return response.status_code
