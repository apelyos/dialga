﻿import datetime

from django import forms
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.core.validators import RegexValidator
from django.db import transaction
from django.http import HttpResponse, Http404
from django.shortcuts import get_object_or_404, render, redirect
from django.template import RequestContext, loader

from schedz.tasks import google_calendar_mgmt
from .models import Slot, GlobalMessage, Schedule, SchedUser

DAYS = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']
HRS_IN_DAY = 24
DYS_IN_WEEK = 7


#################### UTILS ####################

def get_default_schedule():
    schedules = Schedule.objects.filter(is_default=True)
    if (schedules):
        return schedules[0]
    else:
        fallback = Schedule.objects.all()
        if (fallback):
            return fallback[0]
        else:
            raise Http404("No schedules found.")


def week_start_date(d):
    return d - datetime.timedelta((d.weekday() - 6) % 7)  # them calc are for sunday


def week_total_signups(schedule, user, date):
    return Slot.objects.filter(related_schedule=schedule, week_start=date, assigned_user=user).count()


def date_from_string(d):
    return datetime.datetime.strptime(d, "%Y-%m-%d").date()


def slot_valid(hour, day):
    return (hour >= 0 and hour < HRS_IN_DAY and day >= 0 and day < DYS_IN_WEEK)


#################### VIEWS ####################

@login_required
def user(request, id, saved=False):
    user = get_object_or_404(User, id=id)
    last_slots = Slot.objects.filter(assigned_user=user).order_by('-week_start')[:10]
    return render(request, 'schedz/user.html', {'user': user, 'last_slots': last_slots, 'saved': saved})


def detail(request, schedule_slug=get_default_schedule().slug, date=datetime.datetime.now().date().isoformat(),
           saved=False):
    # get requested schedule by slug
    schedule = get_object_or_404(Schedule, slug=schedule_slug)

    try:
        adj_date = week_start_date(date_from_string(date))
    except:
        raise Http404("Invalid date requested.")

    week_slots = Slot.objects.filter(week_start=adj_date, related_schedule=schedule, is_permanent=False)
    # init array
    sched = [["" for x in range(DYS_IN_WEEK)] for x in range(HRS_IN_DAY)]

    for slot in week_slots:
        if slot.assigned_user:
            sched[slot.slot_num][slot.day_of_week] = slot  # .assigned_user

    permanent_slots = Slot.objects.filter(related_schedule=schedule, is_permanent=True)

    for slot in permanent_slots:
        sched[slot.slot_num][slot.day_of_week] = slot

    template = loader.get_template('schedz/sched.html')

    # generate dates for days view
    days_with_dates = []
    for counter, day in enumerate(DAYS):
        curr_date = adj_date + datetime.timedelta(days=counter)
        days_with_dates.append("{} ({})".format(day, curr_date.strftime("%d/%m")))

    context = RequestContext(request, {
        'week_days_name': days_with_dates,
        'schedule': schedule,
        'all_schedules': Schedule.objects.all(),
        'human_week_date': adj_date.strftime("%d/%m/%Y"),
        'week_date': adj_date.isoformat(),
        'sched': sched,
        'next_week': (adj_date + datetime.timedelta(days=DYS_IN_WEEK)).isoformat(),
        'prev_week': (adj_date - datetime.timedelta(days=DYS_IN_WEEK)).isoformat(),
        'saved': saved,
        'max_slots': schedule.max_slots_allowed_in_week,
        'announcements': GlobalMessage.objects.filter(related_schedule=schedule),
    })

    return HttpResponse(template.render(context))


@login_required
def rsvp(request, schedule_slug, date):
    schedule = get_object_or_404(Schedule, slug=schedule_slug)

    date = date_from_string(date)
    req_slots = []
    del_slots = []

    if date < week_start_date(datetime.datetime.now().date()):
        return error_view(request, "Don't modify a past week, dumbass.")

    # signup opening check
    (day, tod) = schedule.signup_opening_time.split('@')
    (hours, mins) = tod.split(':')
    signup_opening = datetime.datetime.combine(
        date - datetime.timedelta(days=DYS_IN_WEEK) + datetime.timedelta(days=int(day)),
        datetime.time(int(hours), int(mins)))
    if datetime.datetime.now() < signup_opening:
        return error_view(request, "Signup for timeslots begins: {:%d/%m/%Y at %H:%M}".format(signup_opening))

    # get/parse reqs, dels
    for (k, v) in request.POST.items():
        if k.startswith('reqslot'):
            req_slots.append(k)
        if k.startswith('delslot'):
            del_slots.append(k)
            # check'em
    if len(req_slots + del_slots) <= 0:
        return error_view(request, "Please select at least one slot.")

    # handle dels
    for del_slot in del_slots:
        (hour, day) = del_slot.split(":")[1].split(',')
        hour = int(hour)
        day = int(day)

        if not slot_valid(hour, day):
            return error_view(request, "Slot validation failed")

        with transaction.atomic():
            slot, created = Slot.objects.get_or_create(related_schedule=schedule, week_start=date, day_of_week=day,
                                                       slot_num=hour)
            if slot.assigned_user == request.user:
                slot.assigned_user = None
                slot.save()
                google_calendar_mgmt(request.user, schedule, slot, action='delete').start()  ### Thread

    # Handle reqs
    if (len(req_slots) + week_total_signups(schedule, request.user, date)) > schedule.max_slots_allowed_in_week:
        return error_view(request,
                          "Can't sign up for more than %d slots in a single week." % schedule.max_slots_allowed_in_week)

    for req_slot in req_slots:
        (hour, day) = req_slot.split(":")[1].split(',')
        hour = int(hour)
        day = int(day)

        if not slot_valid(hour, day):
            return error_view(request, "Slot validation failed")

        with transaction.atomic():
            slot, created = Slot.objects.get_or_create(related_schedule=schedule, week_start=date, day_of_week=day,
                                                       slot_num=hour)

            if slot.assigned_user:
                return error_view(request, "Slot already taken. sorry.")
            else:
                slot.assigned_user = request.user
                slot.save()
                google_calendar_mgmt(request.user, schedule, slot, action='create').start()  ### Thread

    return HttpResponse(detail(request, schedule_slug, date.isoformat(), saved=True))  # return to the detail view


def invalid(request):
    return error_view(request, "Invalid URL Requested or bad week date.")


def error_view(request, err_msg):
    return render(request, "schedz/error.html", {'error': err_msg})


class PhoneForm(forms.Form):
    phone = forms.CharField(label='phone', max_length=12, required=True, validators=[RegexValidator(
        regex='^[-\d]*$',
        message='Phone must be valid',
        code='invalid_phone')], )


def collect_phone(request):
    if request.method == 'POST':
        form = PhoneForm(request.POST)
        if form.is_valid():
            request.session['phone'] = form.cleaned_data['phone']
            request.session['partial_pipeline']['phone'] = form.cleaned_data['phone']
            return redirect(reverse('social:complete', args=("google-oauth2",)))
    else:
        form = PhoneForm()

    return render(request, "schedz/phone_form.html", {'form': form})


class UserEditForm(PhoneForm):
    auto_sched = forms.BooleanField(label='auto_sched')


@login_required
def edit_user(request, id):
    user = get_object_or_404(User, id=id)
    sched_user = get_object_or_404(SchedUser, user_relation=user)

    default_data = {'phone': sched_user.phone_number, 'auto_sched': sched_user.auto_schedule}
    form = UserEditForm(default_data)
    if user == request.user:
        return render(request, 'schedz/user_edit.html', {'user': user, 'form': form})
    else:
        return error_view(request, "Don't fool around.")


@login_required
def edit_user_save(request, id):
    auth_user = get_object_or_404(User, id=id)
    sched_user = get_object_or_404(SchedUser, user_relation=auth_user)
    form = PhoneForm(request.POST)
    if auth_user == request.user:
        if form.is_valid():
            # import pdb; pdb.set_trace()
            sched_user.phone_number = form.cleaned_data['phone']
            if 'auto_sched' in request.POST:
                sched_user.auto_schedule = True
            else:
                sched_user.auto_schedule = False
            sched_user.save()
            return HttpResponse(user(request, id, saved=True))
        else:
            return error_view(request, "Error validating the form. Please enter valid values.")
    else:
        return error_view(request, "Don't fool around.")
