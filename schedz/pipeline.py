from django.shortcuts import redirect
from django.contrib.auth.models import User
from social.pipeline.partial import partial
from django.conf import settings
from .models import SchedUser


@partial
def collect_phone_num(strategy, backend, request, details, *args, **kwargs):
    user = User.objects.get(email=details['email'])
    sched_user, created = SchedUser.objects.get_or_create(user_relation=user)

    if not sched_user.phone_number:
        if not strategy.session_get('phone'):
            return redirect("schedz.views.collect_phone")

        sched_user.phone_number = strategy.session_get('phone')
        sched_user.save()

    # continue the pipeline
    return
