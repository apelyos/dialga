from django.conf.urls import url, include

from . import views

urlpatterns = [
    url(r'^$', views.detail, name='detail'),
    url(r'^user/(?P<id>\d+)/?$', views.user, name='user'),
    url(r'^user/(?P<id>\d+)/edit/$', views.edit_user, name='edit_user'),
    url(r'^user/(?P<id>\d+)/edit/save/$', views.edit_user_save, name='edit_user_save'),
    url(r'^(?P<schedule_slug>[\w-]+)/?$', views.detail, name='detail'),
    url(r'^(?P<schedule_slug>[\w-]+)/(?P<date>\d{4}-\d{2}-\d{2})/?$', views.detail, name='detail'),
    url(r'^(?P<schedule_slug>[\w-]+)/(?P<date>\d{4}-\d{2}-\d{2})/rsvp/$', views.rsvp, name='rsvp'),
    url(r'^.*$', views.invalid, name='invalid'),

]
