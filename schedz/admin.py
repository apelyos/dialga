from django.contrib import admin

# Register your models here.

from .models import Slot, SchedUser, GlobalMessage, Schedule


class SlotAdmin(admin.ModelAdmin):
    list_filter = ('is_permanent', 'related_schedule')


class ScheduleAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("name",)}


admin.site.register(Schedule, ScheduleAdmin)
admin.site.register(SchedUser)
admin.site.register(GlobalMessage)
admin.site.register(Slot, SlotAdmin)
