$(document).ready(function () {
    // Init
    $(':checkbox:checked').prop('checked', false);
    $('span.glyphicon').hide();
    $(':checkbox').hide();
    $('input.btn').prop('disabled', true);

    // Hook
    $('td.available,td.assigned.tome').on('click', function (e) {
        if (!$(e.target).is('input,a')) {
            if ($(this).is('.assigned.tome.selected') && !(numSelected()  < MAX_SLOTS)) {
                modalAlert();
                return false;
            }

            if (!$(this).is('.selected,.assigned.tome') && !checkSlotLimit()) {
                modalAlert();
                return false;
            }

            var Id = $(this).attr('id');
            if (!Id) {
                Id = $(this).closest('td').attr('id');
            }
            //alert(Id);
            //alert(e.target);
            toggleCheckbox(Id);
            $(this).toggleClass('selected');
            $(this).find('span').slideToggle();
            

            if ($('td.selected').length > 0) {
                $('input.btn').prop('disabled', false);
            } else {
                $('input.btn').prop('disabled', true);
            }

            return false;
        }
    });
});

function toggleCheckbox(id) {
    
    var elem = $(":checkbox[id='" + id + "']");
    //alert('bef toggle ' + id + elem.prop('checked'))
    elem.prop('checked', !elem.prop('checked'));
    //alert('af toggle ' + id + elem.prop('checked'))
}

function numSelected() {
    return ($('td.assigned.tome:not(.selected)').length
        - $('td.assigned.tome.selected').length
        + $('td.selected').length);
}

function checkSlotLimit() {
    return (numSelected() < MAX_SLOTS);
}

function modalAlert() {
    $('#errModal').modal('show');
}