# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('schedz', '0004_auto_20150902_1222'),
    ]

    operations = [
        migrations.AlterField(
            model_name='slot',
            name='assigned_user',
            field=models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.DeleteModel(
            name='User',
        ),
    ]
