# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('schedz', '0010_auto_20150905_1442'),
    ]

    operations = [
        migrations.AlterField(
            model_name='scheduser',
            name='uid',
            field=models.UUIDField(default=uuid.uuid4, editable=False),
        ),
        migrations.AddField(
            model_name='scheduser',
            name='id',
            field=models.AutoField(auto_created=True, primary_key=True,  verbose_name='ID', serialize=False),
            preserve_default=False,
        ),

    ]
