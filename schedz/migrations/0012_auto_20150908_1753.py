# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('schedz', '0011_auto_20150905_1537'),
    ]

    operations = [
        migrations.AlterField(
            model_name='scheduser',
            name='phone_number',
            field=models.CharField(max_length=12),
        ),
    ]
