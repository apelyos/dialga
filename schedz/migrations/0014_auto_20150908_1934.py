# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('schedz', '0013_permanentslot'),
    ]

    operations = [
        migrations.DeleteModel(
            name='PermanentSlot',
        ),
        migrations.AddField(
            model_name='slot',
            name='description',
            field=models.CharField(null=True, blank=True, max_length=20),
        ),
        migrations.AddField(
            model_name='slot',
            name='is_permanent',
            field=models.BooleanField(default=False),
        ),
    ]
