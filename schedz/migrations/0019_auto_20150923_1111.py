# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('schedz', '0018_auto_20150923_1024'),
    ]

    operations = [
        migrations.AddField(
            model_name='schedule',
            name='slug',
            field=models.SlugField(default='changeme'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='globalmessage',
            name='message',
            field=models.TextField(max_length=400),
        ),
        migrations.AlterField(
            model_name='schedule',
            name='description',
            field=models.TextField(null=True, max_length=200, blank=True),
        ),
        migrations.AlterField(
            model_name='schedule',
            name='name',
            field=models.CharField(max_length=20, unique=True),
        ),
    ]
