# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('schedz', '0021_slot_google_event_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='schedule',
            name='max_slots_allowed_in_week',
            field=models.IntegerField(default=2),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='schedule',
            name='signup_opening_time',
            field=models.CharField(help_text="format: 'day@time' # 0-sunday, 1-monday etc. example: 3@20:00", max_length=14, default='3@20:00'),
            preserve_default=False,
        ),
    ]
