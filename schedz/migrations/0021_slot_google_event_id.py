# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('schedz', '0020_auto_20150923_1114'),
    ]

    operations = [
        migrations.AddField(
            model_name='slot',
            name='google_event_id',
            field=models.CharField(blank=True, max_length=30, null=True),
        ),
    ]
