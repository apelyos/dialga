# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('schedz', '0003_auto_20150901_1905'),
    ]

    operations = [
        migrations.AlterField(
            model_name='slot',
            name='assigned_user',
            field=models.ForeignKey(blank=True, null=True, to='schedz.User'),
        ),
    ]
