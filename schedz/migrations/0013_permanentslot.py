# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('schedz', '0012_auto_20150908_1753'),
    ]

    operations = [
        migrations.CreateModel(
            name='PermanentSlot',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('day_of_week', models.IntegerField()),
                ('slot_num', models.IntegerField()),
                ('description', models.CharField(max_length=20)),
                ('modified', models.DateTimeField(auto_now=True)),
            ],
        ),
    ]
