# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('schedz', '0015_globalmessage'),
    ]

    operations = [
        migrations.AlterField(
            model_name='globalmessage',
            name='message',
            field=models.CharField(max_length=200),
        ),
    ]
