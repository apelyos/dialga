# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('schedz', '0009_auto_20150905_1442'),
    ]

    operations = [
        migrations.AlterField(
            model_name='scheduser',
            name='uid',
            field=models.UUIDField(default=uuid.uuid4, editable=False, serialize=False, primary_key=True),
        ),	
    ]
