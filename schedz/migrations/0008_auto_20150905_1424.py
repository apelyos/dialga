# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('schedz', '0007_auto_20150904_1349'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='scheduser',
            name='id',
        ),
        migrations.AddField(
            model_name='scheduser',
            name='uid',
            field=models.UUIDField(default=uuid.uuid4, serialize=False),
        ),
    ]
