# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('schedz', '0006_scheduser'),
    ]

    operations = [
        migrations.RenameField(
            model_name='scheduser',
            old_name='supervisor',
            new_name='phone_number',
        ),
        migrations.RenameField(
            model_name='scheduser',
            old_name='user',
            new_name='user_relation',
        ),
    ]
