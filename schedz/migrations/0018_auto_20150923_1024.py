# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('schedz', '0017_schedule'),
    ]

    operations = [
        migrations.AddField(
            model_name='globalmessage',
            name='related_schedule',
            field=models.ForeignKey(default=0, to='schedz.Schedule'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='slot',
            name='related_schedule',
            field=models.ForeignKey(default=0, to='schedz.Schedule'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='globalmessage',
            name='message',
            field=models.CharField(max_length=400),
        ),
        migrations.AlterField(
            model_name='schedule',
            name='valid_days_list',
            field=models.CommaSeparatedIntegerField(default='0,1,2,3,4,5,6', max_length=15),
        ),
        migrations.AlterField(
            model_name='schedule',
            name='valid_times_range',
            field=models.CharField(default='00:00-23:00', max_length=14),
        ),
    ]
