from social.apps.django_app.middleware import SocialAuthExceptionMiddleware
from social import exceptions as social_exceptions
from django.http import HttpResponse, Http404
from schedz.views import error_view


class SocialAuthExceptionMiddleware(SocialAuthExceptionMiddleware):
    def process_exception(self, request, exception):
        # import pdb; pdb.set_trace()

        return error_view(request, exception)

        # if hasattr(social_exceptions, 'AuthCanceled'):
        #	return HttpResponse("I'm the Pony %s" % exception)
        # else:
        #	raise exception
