﻿import datetime
from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
import uuid

# Create your models here.

RECENTLY_HOURS_DELTA = datetime.timedelta(hours=6)
SLOT_DURATION = datetime.timedelta(hours=1)


class Schedule(models.Model):
    name = models.CharField(max_length=20, blank=False, null=False)
    slug = models.SlugField(unique=True)
    description = models.TextField(max_length=200, blank=True, null=True)
    location = models.CharField(max_length=30, blank=True, null=True)
    max_slots_allowed_in_week = models.IntegerField()
    signup_opening_time = models.CharField(max_length=14,
                                           help_text="format: 'day@time' # 0-sunday, 1-monday etc. example: 3@20:00")  #
    valid_days_list = models.CommaSeparatedIntegerField(default="0,1,2,3,4,5,6", max_length=15)
    valid_times_range = models.CharField(default="00:00-23:00", max_length=14)
    is_default = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "{}".format(self.name)


class GlobalMessage(models.Model):
    message = models.TextField(max_length=400, blank=False, null=False)
    posted_by = models.ForeignKey(User, limit_choices_to={'is_staff': True}, blank=False, null=False)
    modified = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)
    related_schedule = models.ForeignKey(Schedule)

    def __str__(self):
        return "{} | BY: {}".format(self.message, self.posted_by.email)


class SchedUser(models.Model):
    uid = models.UUIDField(default=uuid.uuid4, editable=False)
    user_relation = models.OneToOneField(User, on_delete=models.CASCADE)
    phone_number = models.CharField(max_length=12)
    auto_schedule = models.BooleanField(default=False)

    def __str__(self):
        return "{} | {}".format(self.user_relation.email, self.phone_number)


class Slot(models.Model):
    week_start = models.DateField()
    day_of_week = models.IntegerField()
    related_schedule = models.ForeignKey(Schedule)
    slot_num = models.IntegerField()
    popularity = models.IntegerField(default=0)
    assigned_user = models.ForeignKey(User, blank=True, null=True)
    modified = models.DateTimeField(auto_now=True)
    description = models.CharField(max_length=20, blank=True, null=True)
    is_permanent = models.BooleanField(default=False)
    google_event_id = models.CharField(max_length=30, blank=True, null=True)

    def abs_start_time(self):
        return datetime.datetime.combine(self.week_start + datetime.timedelta(days=self.day_of_week),
                                         datetime.time(hour=self.slot_num))

    def abs_end_time(self):
        return datetime.datetime.combine(self.week_start + datetime.timedelta(days=self.day_of_week),
                                         datetime.time(hour=self.slot_num + 1))

    def __str__(self):
        return "{:%d.%m.%Y} {:02d}:00 @ {}".format(self.week_start + datetime.timedelta(days=self.day_of_week),
                                                   self.slot_num, self.related_schedule.name)

    def was_modified_recently(self):
        return self.modified >= timezone.now() - RECENTLY_HOURS_DELTA
